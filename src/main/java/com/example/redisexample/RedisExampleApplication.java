package com.example.redisexample;

import com.example.redisexample.model.Student;
import com.example.redisexample.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.UUID;

@SpringBootApplication
public class RedisExampleApplication implements CommandLineRunner {

    @Autowired
    private StudentService studentService;

    public static void main(String[] args) {
        SpringApplication.run(RedisExampleApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        Student student = new Student();
        student.setId(UUID.randomUUID());
        student.setGender(Student.Gender.MALE);
        student.setName("Fikret");
        studentService.create(student);

        Thread.sleep(200);

        System.out.println(studentService.findAll());
    }
}
