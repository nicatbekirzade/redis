package com.example.redisexample.model;

import lombok.Data;
import org.springframework.data.redis.core.RedisHash;

import java.io.Serializable;
import java.util.UUID;

@Data
@RedisHash("Student")
public class Student implements Serializable {

    private static final long serialVersionUID = 58432132465192L;

    public enum Gender {
        MALE, FEMALE
    }

    private UUID id;
    private String name;
    private Gender gender;
    private int grade;
    // ...
}