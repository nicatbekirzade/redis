package com.example.redisexample.service.impl;

import com.example.redisexample.model.Student;
import com.example.redisexample.repository.StudentRepository;
import com.example.redisexample.service.StudentService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Slf4j
@Service
@RequiredArgsConstructor
public class StudentServiceImpl implements StudentService {

    private final StudentRepository studentRepository;

    @Override
    public Student create(Student student) {
        return studentRepository.save(student);
    }

    @Override
    public Student findById(UUID id) {
        return studentRepository.findById(id).orElseThrow();
    }

    @Override
    public Student update(Student student, UUID id) {
        //must get from db then setId and save
        student.setId(id);
        return studentRepository.save(student);
    }

    @Override
    public void delete(UUID id) {
        studentRepository.deleteById(id);
    }

    @Override
    public List<Student> findAll() {
        return studentRepository.findAll();
    }
}
