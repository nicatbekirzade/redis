package com.example.redisexample.service;

import com.example.redisexample.model.Student;

import java.util.List;
import java.util.UUID;

public interface StudentService {

    Student create(Student student);

    Student findById(UUID id);

    Student update(Student student, UUID id);

    void delete(UUID id);

    List<Student> findAll();
}
